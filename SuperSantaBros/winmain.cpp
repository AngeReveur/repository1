#include "global.h"
#include "game.h"

//#include <dwmapi.h>
//#pragma comment(lib, "dwmapi.lib")


Game game;
// Starting Point
int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
   // GAMEWINDOW gw;

	std::stringstream ss(lpCmdLine);
	int width, height;
	width = height = 0;
	char fullScreen;
	
	if (lpCmdLine != NULL)
	{
	
	ss >> width;
	ss >> height;
	ss >> fullScreen;
	}

	if (width > 0)
	{
		game.gw.Width = width;
		game.gw.Height = height;

		if (fullScreen == 't' || fullScreen == 'T')
			game.gw.Windowed = false;
	}

    DisplayWindow(&game.gw, hInstance, nCmdShow);

	game.InitDirect3D(&game.gw);
    //InitDirect3D(&game.gw);
   // InitDirectInput(hInstance, &game.gw);
	game.InitDirectInput(hInstance,&game.gw);

   // LoadGraphics(); moved in MainLoop

    MainLoop();

	game.CloseDirectInput();
	game.CloseDirect3D();
   // CloseDirect3D();
    //CloseDirectInput();

    return 0;
}

// Create the Window Class and the Window
void DisplayWindow(Game::GAMEWINDOW* gw, HINSTANCE hInstance, int nCmdShow)
{
    WNDCLASSEX wc;

    ZeroMemory(&wc, sizeof(WNDCLASSEX)); wc.cbSize = sizeof(WNDCLASSEX);
    wc.lpfnWndProc = (WNDPROC)WindowProc;
    wc.hInstance = hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.lpszClassName = TEXT("WindowClass");

    RegisterClassEx(&wc);

    gw->hWnd = CreateWindowEx(NULL,
                              wc.lpszClassName,
                              TEXT("Super Santa Bros"),
							  gw->Windowed ? WS_OVERLAPPEDWINDOW: WS_EX_TOPMOST | WS_POPUP,
                              0, 0,
                              gw->Width, gw->Height,
                              NULL,
                              NULL,
                              hInstance,
                              NULL);

	
   // MARGINS                 g_mgDWMMargins = {-1, -1, -1, -1};
	

	//DwmExtendFrameIntoClientArea(gw->hWnd, &g_mgDWMMargins);
	
    ShowWindow(gw->hWnd, nCmdShow);
    return;
}

// Check for Messages and Handle
bool HandleMessages()
{
    static MSG msg;

    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
        if (msg.message == WM_QUIT)
            return FALSE;

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return TRUE;
}

// Process the Messages
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch(message)
    {
        case WM_DESTROY:
            {
                PostQuitMessage(0);
                return 0;
            } break;
    }

    return DefWindowProc (hWnd, message, wParam, lParam);
}

// The Main Loop
void MainLoop()
{
	game.LoadGraphics();
	game.InitGame();
 
	
    while(HandleMessages())
    {
		
		game.Input();
		game.Logic();
		game.Render();
        //Logic(GameObject, &InputData);
       // Render(GameObject);
    }
}