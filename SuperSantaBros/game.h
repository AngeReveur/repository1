#ifndef GAME_H
#define GAME_H
#include<iostream>
class Game{
private:
	LPDIRECT3D9 d3d ;
	LPDIRECT3DDEVICE9 d3ddev ;
	D3DPRESENT_PARAMETERS d3dpp;
	LPD3DXSPRITE d3dspt;
	float testThunder;
	struct GAMESETTING{
	float boxAnimation;
	

	
	} GameSetting;
	struct SPRITE
{
    int rows, cols;
    int width, height;
	char type;
    LPDIRECT3DTEXTURE9 tex;
};
	SPRITE sprites[50];
	class OBJECT
{
public:
    float x, y,z,angle,transparency,rotation;
    float frame;
    float speed;
	int type,maxX,minX,GoombaType;
	bool jumping;
	bool moving;
	bool stolen;
	float velocityY;
	float velocityX;
	int followerIndex;
	int followingIndex;
	int StealPresentIndex;
	bool allowGoInPipe;
	
	SPRITE sprite;

	void Move()
	{
		float scaleX=cos(D3DXToRadian(angle));
		float scaleY=sin(D3DXToRadian(angle));

		velocityX=speed*scaleX;
		velocityY=speed*scaleY;

		this->x+=velocityX;
		this->y+=velocityY;
	}

	void MoveUp()
	{
		//float scaleX=cos(D3DXToRadian(angle));
		float scaleY=sin(D3DXToRadian(angle));

		//velocityX=speed*scaleX;
		velocityY=speed*scaleY;

		//this->x+=velocityX;
		this->y+=velocityY;
	}
};
	OBJECT go[2000];
	OBJECT particles[2000];


	struct GAMEMESSAGE
{
    float x, y,z;
	std::string text;
};

GAMEMESSAGE gm[10];
int score;
int scoreToShow;
int rainParticlesToShow;
int level;
float WorldX;
float WorldY;
int GoombaTestIndex;
int PresentTestIndex;
int countGoombaType1;
int countGoombaType2;
int countGoombaType3;
int ChristmasTreeCount;

	struct INPUTDATA
{
    bool Accelerate;
    bool Brake;
    bool TurnLeft;
    bool TurnRight;

	bool Up;
	bool Right;
	bool Left;
	bool Down;
	bool Space;
	bool SpaceWasDown;
	bool SpacePressed;
	bool Add;
	bool Substract;
	bool Q;
	bool ESC;
	bool W;
};
	INPUTDATA InputData;
public:
	struct GAMEWINDOW
{
    HWND hWnd;
    int Width, Height;
    bool Windowed;

    GAMEWINDOW()
    {
        hWnd = NULL;
        Width = 800;
        Height = 460;
        Windowed = true;
    }
};
	GAMEWINDOW gw;
//Constructor
	//Game();
// Direct3D.cpp
	void InitDirect3D(GAMEWINDOW* gw);
	void CloseDirect3D();
	void StartRender();
	void EndRender();
	void LoadSprite(SPRITE* pSprite, LPCTSTR File, int width, int height, int cols, int rows);
	void DrawSprite(SPRITE* pSprite, int Frame, float x, float y, float z,float transparency,float rotation);
	void DrawTextDX(LPCSTR text,int x,int y);
// DirectInput.cpp
	void InitDirectInput(HINSTANCE hInstance, GAMEWINDOW* gw);
	void GetKeys(BYTE* KeyState);
	void GetMouse(DIMOUSESTATE* MouseState);
	void CloseDirectInput(void);
// Render.cpp
	void Render();
	void LoadGraphics();
// Logic.cpp
	void Logic();
	void InitGame();
	bool CollisionDetectionDown(int index);
	void LoadMap();
	void AssignSprite(Game::OBJECT* GameObject,char type);
	void SantaLogic();
	void AddProjectile(int x,int y);
	void ProjectileLogic();
	void AddGoomba(int GoombaType);
	void GoombaLogic();
	bool CollisionDetectionX(int index);
	void DisposeGameObject(int index);
	void ScoreLogic();
	void InitRain();
	void RainLogic();
	void InitSnow();
	void SnowLogic();
	void SantaLogicIntro();
	void TranslateWorld(float x,float y);
	void AddSnowBall(int x, int y, float angle);
	void SnowBallLogic();
	void DetectSnowballCollision();
	RECT GetGameObjectRectangle(int GameObjectIndex);
	void ShowPipeDecoration(int PresentPositionX, int PresentPositionY);
	void AddPipeDecoration(int PipePositionX, int PipePositionY);
	void WorldLogic();
	double getDistance(float dX0, float dY0, float dX1,float dY1);
	float getAngle(float dX0, float dY0, float dX1, float dY1);
	void getStealPresentForGoomba();
// Input.cpp
	void Input();
	
};

#endif