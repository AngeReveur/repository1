#include "global.h"
#include "game.h"

LPDIRECT3D9 d3d = NULL;
LPDIRECT3DDEVICE9 d3ddev = NULL;
D3DPRESENT_PARAMETERS d3dpp;
LPD3DXSPRITE d3dspt;
LPD3DXFONT dxfont;    // the pointer to the font object

// Create Direct3D and the Direct3D Device
void Game::InitDirect3D(GAMEWINDOW* gw)
{
    d3d = Direct3DCreate9(D3D_SDK_VERSION);

    ZeroMemory(&d3dpp, sizeof(d3dpp));
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
    d3dpp.Windowed = gw->Windowed;
    d3dpp.BackBufferWidth = gw->Width;
    d3dpp.BackBufferHeight = gw->Height;
	//d3dpp.MultiSampleType=D3DMULTISAMPLE_4_SAMPLES;
	//d3dpp.EnableAutoDepthStencil = TRUE;
	//d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_DEFAULT;
    d3d->CreateDevice(D3DADAPTER_DEFAULT,
                      D3DDEVTYPE_HAL,
                      gw->hWnd,
                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                      &d3dpp,
                      &d3ddev);

    D3DXCreateSprite(d3ddev, &d3dspt);



	//Create Font 
	 D3DXCreateFont(d3ddev,    // the D3D Device
                   30,    // font height of 30
                   0,    // default font width
                   FW_NORMAL,    // font weight
                   1,    // not using MipLevels
                   true,    // italic font
                   DEFAULT_CHARSET,    // default character set
                   OUT_DEFAULT_PRECIS,    // default OutputPrecision,
				   ANTIALIASED_QUALITY,    // default Quality
                   DEFAULT_PITCH | FF_DONTCARE,    // default pitch and family
                   "Arial",    // use Facename Arial
                   &dxfont);    // the font object

    return;
}

// Close the Device and Direct3D
void Game::CloseDirect3D()
{
    d3ddev->Release();
    d3d->Release();

    return;
}

// Start rendering
void Game::StartRender()
{   testThunder++;

 if(testThunder>=60*5)
 {
    d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(255,255, 255), 1.0f, 0);
 }
 else
	 if(testThunder>=60*5+5)

 d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(266, 140, 255), 1.0f, 0);
	 else
		 if(testThunder>=60*5+10)
			  d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(255,255, 255), 1.0f, 0);
		 else
			  d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

// d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
 if(testThunder>=60*5+15)
	 testThunder=0;
	 


	//d3ddev->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, 0);


    d3ddev->BeginScene();
    d3dspt->Begin(D3DXSPRITE_ALPHABLEND|D3DXSPRITE_SORT_DEPTH_BACKTOFRONT);
	 
    return;
}

// Stop rendering
void Game::EndRender()
{
    d3dspt->End();
    d3ddev->EndScene();
    d3ddev->Present(NULL, NULL, NULL, NULL);

    return;
}

// Load a graphic into a SPRITE object
void Game::LoadSprite(SPRITE* pSprite, LPCTSTR File, int width, int height, int cols, int rows)
{
    D3DXCreateTextureFromFileEx(d3ddev,
                                File,
                                D3DX_DEFAULT,
                                D3DX_DEFAULT,
                                D3DX_DEFAULT,
                                NULL,
                                D3DFMT_A8R8G8B8,
                                D3DPOOL_MANAGED,
                                D3DX_DEFAULT,
                                D3DX_DEFAULT,
                                D3DCOLOR_XRGB(255, 0, 255),
                                NULL,
                                NULL,
                                &pSprite->tex);

    pSprite->width = width;
    pSprite->height = height;
    pSprite->cols = cols;
    pSprite->rows = rows;

    return;
}

// Draw a frame from a SPRITE object
void Game::DrawSprite(SPRITE* pSprite, int Frame, float x, float y, float z,float transparency,float rotation)
{
	float scale=gw.Height/460.0f;
    RECT FrameBox;
    FrameBox.left = (Frame % pSprite->cols) * pSprite->width;
    FrameBox.top = (Frame / pSprite->cols) * pSprite->height;
    FrameBox.right = FrameBox.left + pSprite->width;
    FrameBox.bottom = FrameBox.top + pSprite->height;

	D3DXMATRIX matScaling;
	D3DXMATRIX matRotation;
	D3DXMATRIX matTranslation;
	
	D3DXMatrixScaling(&matScaling,scale,scale,1.0f);
	D3DXMatrixRotationZ(&matRotation,rotation);
	D3DXMatrixTranslation(&matTranslation,x,y,z);
	d3dspt->SetTransform(&(matRotation*matTranslation*matScaling));

	//D3DXMatrixTransformation2D(

    D3DXVECTOR3 position(x, y, z);
	D3DXVECTOR3 center(28,4,0);
	d3dspt->Draw(pSprite->tex, &FrameBox, &center, NULL, D3DCOLOR_ARGB((int)(255-transparency),255, 255, 255));
	
    return;
}

void Game::DrawTextDX(LPCSTR text,int x,int y)
{
	float scale=gw.Height/460.0f;
	x *= scale;
	y *= scale;
	 // create a RECT to contain the text
    static RECT textbox; SetRect(&textbox, x, y, 500+x, 120+y); 
		
    // draw the Hello World text
    dxfont->DrawTextA(NULL,
		              text,
                      strlen(text),
                      &textbox,
					  DT_LEFT,/* DT_CENTER | DT_VCENTER*/
                      D3DCOLOR_ARGB(255, 255, 255, 255));

}