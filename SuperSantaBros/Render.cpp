#include "global.h"
#include "game.h"

SPRITE interceptor;
SPRITE back;
SPRITE back1;

void Game::LoadGraphics()
{
	

	
	LoadSprite(&sprites[SpriteType::Land], TEXT("Resources\\land1.png"), 16, 16, 1, 1);
	
	sprites[SpriteType::Land].type='1';

	LoadSprite(&sprites[SpriteType::Santa], TEXT("Resources\\SantaTest3.png"), 175, 61, 2, 2);
	
	sprites[SpriteType::Santa].type='S';

	LoadSprite(&sprites[SpriteType::SantaFlipped], TEXT("Resources\\SantaTest3Flipped.png"), 175, 60, 2, 2);

	LoadSprite(&sprites[SpriteType::Present1], TEXT("Resources\\present2.png"), 16,16, 1, 1);

	LoadSprite(&sprites[SpriteType::Pipe0], TEXT("Resources\\pipe2.png"), 32,32, 1, 1);

	sprites[SpriteType::Pipe0].type='|';

	LoadSprite(&sprites[SpriteType::Pipe1], TEXT("Resources\\pipe1.png"), 32,32, 1, 1);

	sprites[SpriteType::Pipe1].type='p';

	LoadSprite(&sprites[SpriteType::Goomba], TEXT("Resources\\GoombaTest1.png"), 32,32, 11, 1);

	sprites[SpriteType::Goomba].type='G';

	LoadSprite(&sprites[SpriteType::Rain], TEXT("Resources\\rain2.png"), 16, 16, 1, 1);

	 LoadSprite(&sprites[SpriteType::RainSplash], TEXT("Resources\\rain3.png"), 16, 16, 1, 1);

	 LoadSprite(&sprites[SpriteType::Snow1], TEXT("Resources\\snow1.png"), 16, 16, 1, 1);

	 LoadSprite(&sprites[SpriteType::SnowBall], TEXT("Resources\\snowball.png"), 32, 32, 1, 1);

	 LoadSprite(&sprites[SpriteType::Exclamation1], TEXT("Resources\\Exclamation1.png"), 64, 64, 1, 1);
	
	 LoadSprite(&sprites[SpriteType::Canon1], TEXT("Resources\\Canon1.png"), 64, 64, 1, 1);

	 LoadSprite(&sprites[SpriteType::GoombaDie], TEXT("Resources\\GoombaDie1.png"), 32, 32, 13, 1);

	 LoadSprite(&sprites[SpriteType::PipeDecoration1], TEXT("Resources\\PipeDecoration1.png"), 64, 64, 1, 1);

	 LoadSprite(&sprites[SpriteType::PipeDecoration2], TEXT("Resources\\PipeDecoration2.png"), 64, 64, 1, 1);

	 LoadSprite(&sprites[SpriteType::PipeDecoration3], TEXT("Resources\\PipeDecoration3.png"), 64, 64, 1, 1);

	 LoadSprite(&sprites[SpriteType::Cloud1], TEXT("Resources\\Cloud1.png"), 64, 64, 1, 1);
	 sprites[SpriteType::Cloud1].type = '2';

	 LoadSprite(&sprites[SpriteType::Cloud2], TEXT("Resources\\Cloud2.png"), 64, 64, 1, 1);
	 sprites[SpriteType::Cloud2].type = '3';

	 LoadSprite(&sprites[SpriteType::Cloud3], TEXT("Resources\\Cloud3.png"), 64, 64, 1, 1);
	 sprites[SpriteType::Cloud3].type = '4';

	 LoadSprite(&sprites[SpriteType::Bush1], TEXT("Resources\\Bush1.png"), 128, 128, 1, 1);
	 sprites[SpriteType::Bush1].type = '5';

	 LoadSprite(&sprites[SpriteType::Star1], TEXT("Resources\\Star1.png"), 16, 16, 4, 1);
	 sprites[SpriteType::Star1].type = '6';

	 LoadSprite(&sprites[SpriteType::Castle1], TEXT("Resources\\Castle1.png"), 256, 256, 1, 1);
	 sprites[SpriteType::Castle1].type = 'C';

	 LoadSprite(&sprites[SpriteType::WorldDecoration], TEXT("Resources\\Decorations2.png"), 512, 512, 1, 1);
	 sprites[SpriteType::WorldDecoration].type = 'D';
	
	 LoadSprite(&sprites[SpriteType::ChristmasTree1], TEXT("Resources\\ChristmasTree1.png"), 128,128, 1, 1);
	 sprites[SpriteType::ChristmasTree1].type = 'X';

	 LoadSprite(&sprites[SpriteType::GameEndMessage], TEXT("Resources\\EndLevel2.png"), 512, 512, 1, 1);
}

void Game::Render()
{
    StartRender();
	
   for(int i=0;i<2000;i++)
		if(go[i].sprite.width>0)
			DrawSprite(&go[i].sprite, (int)go[i].frame,go[i].x, (int)go[i].y, go[i].z,go[i].transparency,go[i].rotation);

   for(int i=0;i<2000;i++)
		if(particles[i].sprite.width>0)
			DrawSprite(&particles[i].sprite, (int)particles[i].frame, (int)particles[i].x, particles[i].y, particles[i].z,particles[i].transparency,0);


   for(int i=0;i<10;i++)
   {
	   if(gm[i].text!="")
		   DrawTextDX(gm[i].text.c_str(),gm[i].x,gm[i].y);
   }

 	EndRender();
	
    return;
}