
namespace SpriteType {

	enum TSpriteType{Land=0,Santa=1,SantaFlipped=2,Present1=3,Mario=4,Pipe0=5,Pipe1=6,Goomba=7,Rain=8,RainSplash=9,Snow1=10,SnowBall=11,Exclamation1=12,Canon1=13,GoombaDie=14,
	                 PipeDecoration1=15,PipeDecoration2=16,PipeDecoration3=17,ChristmasTree1=18,Cloud1=19,
					 Cloud2=20,Cloud3=21,Bush1=22,Star1=23,Castle1=24,WorldDecoration=25,GameEndMessage=26};
}

namespace GameObjectType {

	enum TGameObjectType{Santa=1997,Mario=1999,Goomba=1500,Canon=1998,GameEndMessage=1996};
}

namespace GameObjectStartIndex{
	enum TGameObjectLimitIndex{World=0,Presents=1000,Goomba=1500,SnowBall=1700,Decorations=1800,GoombaPipe=1650};
}

namespace GameObjectLimitIndex{
	enum TGameObjectLimitIndex{World=1000,Presents=1500,Goomba=1650,SnowBall=1800,Decorations=1900,GoombaPipe=1700};
}

namespace GameMessage{
	enum TGameMessage{Score=0,SkipIntro=3};
}

