#include "global.h"
#include "game.h"

void Game::Input()
{
    static BYTE Keys[256];
    //static DIMOUSESTATE Mouse;    // We won't need the mouse for this

    GetKeys(&Keys[0]);
    //GetMouse(&Mouse);

    ZeroMemory(&InputData, sizeof(INPUTDATA));

	if(Keys[DIK_UP])
		InputData.Up=true;
	if(Keys[DIK_DOWN])
		InputData.Down=true;

	if(Keys[DIK_LEFT])
		InputData.Left=true;
	if(Keys[DIK_RIGHT])
		InputData.Right=true;

	if(Keys[DIK_SPACE])
		InputData.Space=true;


	if(Keys[DIK_ADD])
		InputData.Add=true;

	if(Keys[DIK_SUBTRACT])
		InputData.Substract=true;

	if (Keys[DIK_Q])
		InputData.Q = true;

	if (Keys[DIK_ESCAPE])
		InputData.ESC = true;

	if (Keys[DIK_W])
		InputData.W = true;


    //if(Keys[DIK_UP] || Keys[DIK_W])    // Up or W
    //    InputData->Accelerate = true;
    //if(Keys[DIK_DOWN] || Keys[DIK_S])
    //    InputData->Brake = true;
    //if(Keys[DIK_LEFT] || Keys[DIK_A])
    //    InputData->TurnLeft = true;
    //if(Keys[DIK_RIGHT] || Keys[DIK_D])
    //    InputData->TurnRight = true;

    return;
}