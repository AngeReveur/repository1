#include "global.h"
#include "game.h"

void Game::InitGame()
{

	for(int i=0;i<1000;i++)
	{
		DisposeGameObject(i);
	}
	WorldY=0;
	LoadMap();

	go[GameObjectType::Santa].sprite=sprites[SpriteType::Santa];
	go[GameObjectType::Santa].x=10;
	go[GameObjectType::Santa].y=380;
	go[GameObjectType::Santa].z=1.0f;
	go[GameObjectType::Santa].speed=0.1f;

	go[GameObjectType::Canon].sprite=sprites[SpriteType::Canon1];
	go[GameObjectType::Canon].z=1.0f;
	go[GameObjectType::Canon].transparency = 255;


	go[GameObjectType::GameEndMessage].sprite = sprites[SpriteType::GameEndMessage];
	go[GameObjectType::GameEndMessage].x = 200;
	go[GameObjectType::GameEndMessage].z = 1.0f;
	go[GameObjectType::GameEndMessage].transparency = 255;
	

	go[GameObjectType::Mario].sprite=sprites[SpriteType::Mario];
	go[GameObjectType::Mario].x=10;
	go[GameObjectType::Mario].y=400;
	go[GameObjectType::Mario].velocityX=0.1f;

	score=0;
	scoreToShow=0;
	rainParticlesToShow=0;
	
	WorldX=0;
	


	rainParticlesToShow=900;
	InitSnow();
	
	
	gm[GameMessage::SkipIntro].x = 200;
	gm[GameMessage::SkipIntro].y = 0;
	gm[GameMessage::SkipIntro].text = "Press Esc to skip intro...";


	
	
}
bool animation=false;
int time0=0;
void Game::Logic()
{
    static int time, start_time = GetTickCount();
    time = GetTickCount() - start_time;
    start_time = GetTickCount();


	

    for(int ms = 0; ms < time; ms++)    // once per millisecond...
    {


		//To remove 
		if (time0 == 0)
			time0 = GetTickCount();

		//if (level == 0)
		if (GetTickCount() - time0>rand() % (1000 - 2000) + 2000)
		{
			if (level == 1 && WorldY <=10)
			{
				AddGoomba(1);
			}			
			else
			if (level == 0)
			{
				InputData.Space = true;
			}
			
		//	InputData.Q = true;
			time0 = GetTickCount();
		}
     //
		if(level==1)
		{
		SantaLogic();
		}
		else
		{
	   SantaLogicIntro();
		}
		ProjectileLogic();

		GoombaLogic();

		ScoreLogic();
		
		SnowLogic();

		SnowBallLogic();

		WorldLogic();

	//	RainLogic();

		
		


		if (InputData.Left&&go[GameObjectType::Canon].rotation<1)
	{
		go[GameObjectType::Canon].rotation+=0.01f;
	}

		if (InputData.Right&&go[GameObjectType::Canon].rotation>-1)
		{
			go[GameObjectType::Canon].rotation -= 0.01f;
		}
		

    }
	
	

    return;
}

boolean allowProjectile=true;
boolean allowSnowBall = true;

void Game::SantaLogic()
{

	if (InputData.W)
	{
		if (go[GameObjectType::Santa].velocityX>0)
		go[GameObjectType::Santa].velocityX = 0.5f;
		else
			go[GameObjectType::Santa].velocityX = -0.5f;
	}
	else
	{
		if (go[GameObjectType::Santa].velocityX>0)
		go[GameObjectType::Santa].velocityX = 0.1f;
		else
			go[GameObjectType::Santa].velocityX = -0.1f;
	}

	if(WorldY>0)
	{
		TranslateWorld(0,-0.1f);
	}

	if(InputData.Space&&allowProjectile)
	{
		allowProjectile=false;
		if(go[GameObjectType::Santa].velocityX>0)
		AddProjectile(go[GameObjectType::Santa].x,go[GameObjectType::Santa].y);
		else
			AddProjectile(go[GameObjectType::Santa].x+go[GameObjectType::Santa].sprite.width,go[GameObjectType::Santa].y);
	}
	else if(!InputData.Space)
	{
		allowProjectile=true;
	}

	if (InputData.Q&&allowSnowBall)
	{
		allowSnowBall = false;
		//AddGoomba(1);
		AddSnowBall(go[GameObjectType::Canon].x, go[GameObjectType::Canon].y, go[GameObjectType::Canon].rotation+D3DX_PI/2);
	}
	else if (!InputData.Q)
	{
		allowSnowBall = true;
	}
	
	if(InputData.Down)
	{go[GameObjectType::Santa].velocityY=0.1f;
	}
	else
		if(InputData.Up)
	{
		
		go[GameObjectType::Santa].velocityY=-0.1f;
	}
		else

	{
		go[GameObjectType::Santa].velocityY=0.0f;
	}

	
	
	


	go[GameObjectType::Santa].x+=go[GameObjectType::Santa].velocityX;

	bool canMove = true;
	
	if (go[GameObjectType::Santa].y<10 && go[GameObjectType::Santa].velocityY<0)
	{
		canMove = false;
	}

	if (go[GameObjectType::Santa].y>300 && go[GameObjectType::Santa].velocityY>0)
	{
		canMove = false;
	}

	if (canMove)
		go[GameObjectType::Santa].y += go[GameObjectType::Santa].velocityY;
	
	
	

	if(go[GameObjectType::Santa].x>600)
		{

		if (InputData.W)
		{
			go[GameObjectType::Santa].velocityX = -1;
		}
		else
		{
			go[GameObjectType::Santa].velocityX = -0.1f;
		}
			
			go[GameObjectType::Santa].sprite=sprites[SpriteType::SantaFlipped];
	}

	if(go[GameObjectType::Santa].x<10)
		{

		if (InputData.W)
		{
			go[GameObjectType::Santa].velocityX = 1;
		}
		else
		{
			go[GameObjectType::Santa].velocityX = 0.1f;
		}
	     go[GameObjectType::Santa].sprite=sprites[SpriteType::Santa];
	    }

	go[GameObjectType::Santa].frame+=0.01f;

	

	if(go[GameObjectType::Santa].frame>3)
		go[GameObjectType::Santa].frame=0;


	// Canon

	if (go[GameObjectType::Canon].transparency > 50)
	{
		go[GameObjectType::Canon].transparency -= 0.07f;

		if (go[GameObjectType::Canon].transparency < 0)
			go[GameObjectType::Canon].transparency = 0;
	}

	if (go[GameObjectType::Santa].sprite.tex == sprites[SpriteType::SantaFlipped].tex)
	{
		go[GameObjectType::Canon].x = go[GameObjectType::Santa].x+120;
		go[GameObjectType::Canon].y = go[GameObjectType::Santa].y + 30;
	}
	else
	{
		go[GameObjectType::Canon].x = go[GameObjectType::Santa].x;
		go[GameObjectType::Canon].y = go[GameObjectType::Santa].y + 30;
	}

	OBJECT santa = go[GameObjectType::Santa];
	//test
//score=	getDistance(santa.x, santa.y, 0, 400);
}

void Game::SantaLogicIntro()
{

	if (InputData.ESC)
	{
		WorldX = 3000;
		go[GameObjectType::Santa].y = 200;
	}

	go[GameObjectType::Santa].frame+=0.01f;

	if(go[GameObjectType::Santa].frame>3)
		go[GameObjectType::Santa].frame=0;

	if(go[GameObjectType::Santa].x<300)
	{
		go[GameObjectType::Santa].Move();

	
	}
	else
	{
		TranslateWorld(-go[GameObjectType::Santa].velocityX,0);

		if(go[GameObjectType::Santa].y>200)
		go[GameObjectType::Santa].MoveUp();
	}

	if(WorldX>440&&go[GameObjectType::Santa].angle==0)
	{
		go[GameObjectType::Santa].angle=-30;
	}


	if(InputData.Space&&allowProjectile)
	{
		allowProjectile=false;
		
		AddProjectile(go[GameObjectType::Santa].x,go[GameObjectType::Santa].y);
		
	}
	else if(!InputData.Space)
	{
		allowProjectile=true;
	}


	if(WorldX>3000)
	{
		// Change Level
		level=1;
		WorldY=500;

		for (int i = 0; i<1000; i++)
		{
			DisposeGameObject(i);
		}
		LoadMap();

		// Dispose Game Message
		ZeroMemory(&gm[GameMessage::SkipIntro], sizeof(gm[GameMessage::SkipIntro]));
		ZeroMemory(&gm[1], sizeof(gm[1]));
	}


	go[GameObjectType::Canon].x=go[GameObjectType::Santa].x;
	go[GameObjectType::Canon].y=go[GameObjectType::Santa].y+30;

	if (time0 == 0)
		time0 = GetTickCount();

	if (level == 0 && go[GameObjectType::Santa].y<=200)
	if (GetTickCount() - time0>rand() % (1000 - 2000) + 2000)
	{

		InputData.Space = true;
		time0 = GetTickCount();
	}
}

void Game::AddProjectile(int x,int y)
{
	int projectileIndex=GameObjectStartIndex::Presents;

	for(int i=GameObjectStartIndex::Presents;i<GameObjectLimitIndex::Presents;i++)
	{
		if(go[i].sprite.width==0)
		{
			projectileIndex=i;
			break;
		}
	}

	go[projectileIndex].sprite=sprites[SpriteType::Present1];
		go[projectileIndex].x=x;
		go[projectileIndex].y=y;
		go[projectileIndex].z=1.0f;
		go[projectileIndex].moving=true;
		go[projectileIndex].type=3;
		go[projectileIndex].speed=0.3f;
		go[projectileIndex].followingIndex = 0;
		if(level==0){
		go[projectileIndex].angle=rand()%170+10;
		}
		else
		{
			go[projectileIndex].angle=90;
		}


		// test
		//go[GoombaTestIndex].StealPresentIndex= projectileIndex;
		
		
}

void Game::ProjectileLogic()
{

	for(int i=GameObjectStartIndex::Presents;i<GameObjectLimitIndex::Presents;i++)
	{

		if(go[i].y>500)
		{
			DisposeGameObject(i);
			if(level==0)
			{
				//score+=300;
    				
			}
		}

	if(go[i].moving)
	{
	  //  go[i].velocityY=0.1f;

		//go[i].y+=go[i].velocityY;
		go[i].Move();
	}

	if(go[i].moving&&CollisionDetectionDown(i ))
		{ 
			go[i].moving=false;
			go[i].y+=3;

			if(!go[i].allowGoInPipe&&level!=0)
			{AddGoomba(0);}
			else
			{
				go[i].z=0.0f;
 				go[i].moving=true;
				score+=300;
				ShowPipeDecoration(go[i].x, go[i].y);

				if(rainParticlesToShow<900)
				{
					rainParticlesToShow+=100;
					InitRain();
				}
			}

		}

	}

}

void Game::DisposeGameObject(int index)
{
	ZeroMemory(&go[index], sizeof(go[index]));
}
void Game::AddGoomba(int GoombaType)
{
	int startIndex=GameObjectStartIndex::Goomba;

	for(int i=startIndex;i<GameObjectLimitIndex::Goomba;i++)
	{
		if(go[startIndex].sprite.width==0)
		{
		go[startIndex].sprite=sprites[SpriteType::Goomba];
		go[startIndex].x=10;
		go[startIndex].y=403;

		// test
		//go[startIndex].x = 200;
		//go[startIndex].y = 363;
		// /test
		go[startIndex].z=1.0f;
		//go[startIndex].velocityX=0.1f;
		go[startIndex].speed = 0.1f;
		go[startIndex].angle = 0;
		go[startIndex].type='G';
		go[startIndex].followerIndex=0;
		if (GoombaType == 0)
		{
			go[startIndex].maxX = 2000;
			go[startIndex].minX = 0;
			GoombaType = 0;
		}
		else
		{
			if (countGoombaType2 < countGoombaType1)
				GoombaType = 2;

			if (countGoombaType3 < countGoombaType2)
				GoombaType = 3;

			if (GoombaType == 1)
			{
			
			go[startIndex].maxX = 220;
			go[startIndex].minX = 0;
			GoombaTestIndex = startIndex;
			go[startIndex].GoombaType = 1;
			countGoombaType1++;
			}

			if (GoombaType == 2)
			{

				go[startIndex].maxX = 420;
				go[startIndex].minX = 270;
				GoombaTestIndex = startIndex;
				go[startIndex].GoombaType = 2;
				countGoombaType2++;
			}

			if (GoombaType == 3)
			{

				go[startIndex].maxX = 650;
				go[startIndex].minX = 470;
				GoombaTestIndex = startIndex;
				go[startIndex].GoombaType = 3;
				countGoombaType3++;
			}


		}
		
		if (GoombaType==0)
		score-=50;
		break;
	}
		startIndex++;
	}
}
 float gravitation=0.008f;
 void Game::GoombaLogic()
 {
	 /*if(InputData.Up)
	 {
	 for(int i=GameObjectType::Goomba;i<1900;i++)
	 {
	 if(go[i].sprite.width==0||go[i].moving)
	 continue;

	 go[i].velocityY=-0.9f;
	 go[i].moving=true;
	 }

	 }*/
	 getStealPresentForGoomba();
	 for (int i = GameObjectType::Goomba; i<GameObjectLimitIndex::Goomba; i++)
	 {
		 if (go[i].sprite.width == 0)
			 continue;


		 if (go[i].GoombaType>0)
		 {

			 if (go[i].y < 403 && go[i].followerIndex != 0)
			 {
				 go[i].angle = D3DXToDegree(getAngle(go[i].x, go[i].y, go[i].x + 20, 403)) + 180;
			 }
			 else
			 if (go[i].y >= 403 && go[i].followerIndex != 0)
			 {
				 go[i].angle = 0;
			 }
		 }

		 go[i].Move();

		

		 if (go[i].x>900||go[i].y<0)
		 {
			 int followerIndex = go[i].followerIndex;

			 if (go[i].type>0 && go[i].followerIndex != 0)
			 {
				 AddGoomba(1);
			 }

			 DisposeGameObject(i);

			 if (followerIndex != 0)
			 {
				 score -= 50;
				 DisposeGameObject(followerIndex);
			 }
			

			
		 }



		 go[i].frame += 0.01f;

		 if (go[i].angle == 0)
		 {
			 if (go[i].frame > go[i].sprite.cols)
				 go[i].frame = 0;
		 }
		 else
		 {
			 if (go[i].frame > go[i].sprite.cols)
				 go[i].frame = 6;

		 }



		 if (CollisionDetectionX(i))
		 {
			 int index = go[i].followerIndex;

			 go[index].x = go[i].x + 15;
			 go[index].y = go[i].y + 8;
			 go[i].speed = 0.3f;

		 }

		 if (go[i].followerIndex == 0)
		 {
		 
		 if (go[i].x > go[i].maxX)
		 {
			 go[i].speed = -0.1f;
		 }

		 if (go[i].x < go[i].minX)
		 {
			 go[i].speed = 0.1f;
		 }

		 // Steal Present

		 if (go[i] .StealPresentIndex!=0&& go[i].followerIndex == 0)
		 {
			 int StealPresentIndex = go[i].StealPresentIndex;
			 go[i].angle = D3DXToDegree(getAngle(go[i].x, go[i].y, go[StealPresentIndex].x, go[StealPresentIndex].y))+180;
			 go[i].maxX = 2000;
			 go[i].speed = 0.6f;
		 }
		 
			 
		 
		

	 }
 }
	
}

bool Game::CollisionDetectionX(int index)
{
	RECT goombaRectangle;
	RECT presentRectangle;

	goombaRectangle = GetGameObjectRectangle(index);
	if(go[index].sprite.width==0)
		return false;

	for(int i=GameObjectStartIndex::Presents;i<GameObjectLimitIndex::Presents;i++)
	{
		if(go[i].type!=3)
			continue;

		int x1=go[index].x+go[index].sprite.width;

		int x2=go[i].x;

		
		presentRectangle = GetGameObjectRectangle(i);
		//if (x1 >= x2 && x1 < x2 + 32 )
		if (!(presentRectangle.bottom<goombaRectangle.top || presentRectangle.top>goombaRectangle.bottom ||
			presentRectangle.left>goombaRectangle.right || presentRectangle.right<goombaRectangle.left))
		{
			if (go[i].followingIndex != 0 && go[i].followingIndex != index)
				continue;

			if (go[index].followerIndex == 0 || go[index].followerIndex == i)
			{
			
 			go[index].followerIndex=i;
			go[i].followingIndex = index;
			go[i].moving = false;
			
			return true;
			}
		}
	}

	return false;
}
bool Game::CollisionDetectionDown(int index)
{
	if (level == 0)
		return false;
	if(go[index].allowGoInPipe)
		return false;

	for(int i=0;i<1024;i++)
	{
		if(go[i].sprite.width==0)
			continue;
	
		if(go[i].type==1||go[i].type==2)
	{
	
	    /*	if(((int)go[1999].y+32>=go[i].y &&(int)go[1999].y+33<=go[i].y+16 )&&((int)go[1999].x+15>=go[i].x&&(int)go[1999].x+15<=go[i].x+16))
			{
				go[1999].y=go[i].y-32;
				return true;
		}*/

		if(((int)go[index].y+(int)go[index].sprite.height>=go[i].y &&(int)go[index].y+16<=go[i].y+go[i].sprite.height )&&((int)go[index].x+15>=go[i].x&&(int)go[index].x+15<=go[i].x+go[i].sprite.width))
			{
				//go[1000].y=go[i].y-32;

				if(go[i].type==2)
				{
					go[index].allowGoInPipe=true;
				}
                              				return true;
		}


	
	}
	
	}
	return false;
}

void Game::LoadMap()
{
	std::ifstream file;

	if(level==0){

	file.open("Resources\\mapTest.txt");
	}
	else
	{file.open("Resources\\Level1.txt");

	}
	char output[1000]="a";
	int line=0;
	int k=0;
	while(!file.eof())
	{
	file>>output;

	for(int i=0;i<1000;i++)
	{
		if(output[i]==48||output[i]==-52||output[i]==0||output[i]==32||output[i]==45)
			continue;
			AssignSprite(&go[k],output[i]);
			if (output[i] == '1')
			{
				go[k].type = 1;
				go[k].z = 0.5f;
			}
			else
            go[k].type=0;

			go[k].x=16*i;
			if(output[i] == '|' || output[i] == 'p')
			{
				go[k].z = 0.5f;
				go[k].type = 2;
			}
			
			go[k++].y=16*line+WorldY;

			if (output[i] == 112) // is Pipe
			{
				AddPipeDecoration(16 * i, 16 * line);
			}
			
			
	}
		
		
	line++;
	}


	file.close();
}

void Game::AssignSprite(Game::OBJECT* GameObject, char type)
{
	for(int i=0;i<50;i++)
	{
	
		if(sprites[i].type==type)
		{
			GameObject->sprite=sprites[i];
			break;
		}

	}

}

void Game::ScoreLogic()
{
	if(scoreToShow<score)
			scoreToShow++;

		if(scoreToShow>score)
			scoreToShow--;
	
	std::string test2="Score ";
	
	std::ostringstream Convert;

	Convert<<scoreToShow;

	std::string a=Convert.str();

	gm[0].x=20;
	gm[0].y=20;
	gm[0].text=test2+a;

	

}

void Game::InitRain()
{
	

	for(int i=0;i<rainParticlesToShow;i++)
	{
		if(particles[i].sprite.width!=0)
			continue;
		
		particles[i].x=rand()%gw.Width;
	particles[i].y=rand()%gw.Height;
	 
	particles[i].sprite=sprites[SpriteType::Rain];
	}

	for(int i=1000;i<1020;i++)
	{
	{particles[i].x=rand()%gw.Width;
	particles[i].y=425;;
	 
	particles[i].sprite=sprites[SpriteType::RainSplash];
	}
	}

}

void Game::RainLogic()
{
	float speedX=0.4f;
	float speedY=0.8f;
	

	for(int i=0;i<rainParticlesToShow;i++)
	{//go[i].x=rand()%gw.Width;
	//go[i].y=rand()%gw.Height;

		particles[i].y+=speedY;
	 particles[i].x-=speedX;
	 if(particles[i].y>=420)
		 {particles[i].y=-rand()%gw.Height-350;
	 particles[i].x=rand()%gw.Width;
	 particles[i].transparency=rand()%50;
	 }
	//go[i].sprite=sprites[3];
	}

	
	   for(int i=1000;i<1020;i++)
	{

		particles[i].transparency+=0.8f;
		if(i%2==0)
		particles[i].y-=0.01f;
		else
			//particles[i].y+=0.1f;
		//if(i%2==0)
		particles[i].x+=0.1f;
		//else
			//particles[i].x-=0.1f;
		if(particles[i].transparency>=255)
		{particles[i].transparency=0;
		particles[i].y=421;
		particles[i].x=rand()%gw.Width;
		}
	   }
}

void Game::InitSnow()
{
	

	for(int i=0;i<rainParticlesToShow;i++)
	{
		if(particles[i].sprite.width!=0)
			continue;
		
		particles[i].x=rand()%gw.Width;
	particles[i].y=rand()%gw.Height;


	particles[i].velocityX=(rand()%3)*0.1f-0.1f;
	if(particles[i].velocityX==0)
		particles[i].velocityX=0.05f;

	particles[i].velocityY=(rand()%2)*0.1f+0.1f;

	 
	particles[i].sprite=sprites[SpriteType::Snow1];
	}

	/*for(int i=1000;i<1020;i++)
	{
	{particles[i].x=rand()%gw.Width;
	particles[i].y=425;;
	 
	particles[i].sprite=sprites[SpriteType::RainSplash];
	}
	}*/

}

void Game::SnowLogic()
{
	float speedX=0.0f;
	float speedY=0.2f;
	

	for(int i=0;i<rainParticlesToShow;i++)
	{//go[i].x=rand()%gw.Width;
	//go[i].y=rand()%gw.Height;

		particles[i].y+=particles[i].velocityY;
		particles[i].x-=particles[i].velocityX;
	 if(particles[i].y>=620)
		 {particles[i].y=-rand()%gw.Height-350;
	 particles[i].x=rand()%gw.Width;
	 particles[i].transparency=rand()%50;
	 }
	//go[i].sprite=sprites[3];
	}

	
	//   for(int i=1000;i<1020;i++)
	//{

	//	particles[i].transparency+=0.8f;
	//	if(i%2==0)
	//	particles[i].y-=0.01f;
	//	else
	//		//particles[i].y+=0.1f;
	//	//if(i%2==0)
	//	particles[i].x+=0.1f;
	//	//else
	//		//particles[i].x-=0.1f;
	//	if(particles[i].transparency>=255)
	//	{particles[i].transparency=0;
	//	particles[i].y=421;
	//	particles[i].x=rand()%gw.Width;
	//	}
	  // }
}


void Game::TranslateWorld(float x,float y)
{
	for(int i=GameObjectStartIndex::World;i<GameObjectLimitIndex::World;i++)
	{
		go[i].x+=x;
		

		go[i].y+=y;
	}

	for(int i=GameObjectStartIndex::Presents;i<GameObjectLimitIndex::Presents;i++)
	{
		go[i].x+=x;
		

		go[i].y+=y;
	}

	
	WorldX-=x;
	WorldY+=y;

	if (level == -1)
	{
	
	std::ostringstream Convert;

	Convert<<"World X:"<<WorldX;

	std::string a=Convert.str();

	gm[1].x=20;
	gm[1].y=200;
	gm[1].text=a;
	}
	
}

void Game::AddSnowBall(int x, int y, float angle)
{
	if (ChristmasTreeCount == 3)
		return;
	int snowBallIndex = GameObjectStartIndex::SnowBall;
 
	for (int i = GameObjectStartIndex::SnowBall; i<GameObjectLimitIndex::SnowBall; i++)
	{
		if (go[i].sprite.width == 0)
		{
			snowBallIndex = i;
			break;
		}
	}

	go[snowBallIndex].sprite = sprites[SpriteType::SnowBall];
	go[snowBallIndex].x = x;
	go[snowBallIndex].y = y+30;
	go[snowBallIndex].angle = D3DXToDegree(angle);
	go[snowBallIndex].speed = 0.5f;

}

void Game::SnowBallLogic()
{
	if (ChristmasTreeCount == 3)
	{
		if (go[GameObjectType::GameEndMessage].transparency > 0)
			go[GameObjectType::GameEndMessage].transparency -= 0.07f;
		if (go[GameObjectType::GameEndMessage].transparency < 0)
			go[GameObjectType::GameEndMessage].transparency = 0;
		return;
	}
		
	for (int i = GameObjectStartIndex::SnowBall; i < GameObjectLimitIndex::SnowBall; i++)
	{
		if (go[i].sprite.width == 0)
			continue;

		go[i].Move();

		if (go[i].y>500)
		{
			DisposeGameObject(i);
		}

		//go[i].x += go[i].velocityX;
		//go[i].y += go[i].velocityY;
	}

	DetectSnowballCollision();
}

void Game::DetectSnowballCollision()
{	
	RECT snowBallRectangle;
	RECT goombaRectangle;


	for (int snowBallIndex = GameObjectStartIndex::SnowBall; snowBallIndex < GameObjectLimitIndex::SnowBall;snowBallIndex++)
	{
		if (go[snowBallIndex].sprite.width == 0)
			continue;

		snowBallRectangle = GetGameObjectRectangle(snowBallIndex);

		for (int goombaIndex = GameObjectStartIndex::Goomba;  goombaIndex< GameObjectLimitIndex::Goomba; goombaIndex++)
		{
			if (go[goombaIndex].sprite.width == 0)
				continue;

			goombaRectangle = GetGameObjectRectangle(goombaIndex);

			if (!(snowBallRectangle.bottom<goombaRectangle.top || snowBallRectangle.top>goombaRectangle.bottom ||
				  snowBallRectangle.left>goombaRectangle.right || snowBallRectangle.right<goombaRectangle.left ))
			{
				go[goombaIndex].sprite = sprites[SpriteType::GoombaDie];
				go[goombaIndex].angle = -90;
				go[goombaIndex].speed = 0.05f;
				go[goombaIndex].GoombaType = 0;
				go[goombaIndex].StealPresentIndex = 0;
			}
		}

	}

}

RECT Game::GetGameObjectRectangle(int GameObjectIndex)
{
	RECT GameObjectRectangle;

	GameObjectRectangle.top = go[GameObjectIndex].y;
	GameObjectRectangle.bottom = GameObjectRectangle.top + go[GameObjectIndex].sprite.height;
	GameObjectRectangle.left = go[GameObjectIndex].x;
	GameObjectRectangle.right = GameObjectRectangle.left + go[GameObjectIndex].sprite.width;
	
	return GameObjectRectangle;
}

void Game::ShowPipeDecoration(int x,int y)
{
	for (int DecorationIndex = GameObjectStartIndex::Decorations; DecorationIndex < GameObjectLimitIndex::Decorations; DecorationIndex++)
	{
		if (go[DecorationIndex].transparency>0)
		{        
			if (go[DecorationIndex].x - x > 100 || go[DecorationIndex].x - x < -100)
			{
				continue;
			}

			if (go[DecorationIndex].type == 'T')
			{
				go[DecorationIndex].transparency = 0;
				ChristmasTreeCount++;
			}

			go[DecorationIndex].transparency -= 135;
			
			if (go[DecorationIndex].transparency < 0)
				go[DecorationIndex].transparency = 0;

			break;
		}

	}
	
}

void Game::AddPipeDecoration(int PipePositionX, int PipePositionY)
{
	int PipeDecorationIndex = GameObjectStartIndex::Decorations;

	for (int i = GameObjectStartIndex::Decorations; i<GameObjectLimitIndex::Decorations; i++)
	{
		if (go[i].sprite.width == 0)
		{
			PipeDecorationIndex = i;
			break;
		}
	}

	
	go[PipeDecorationIndex].sprite = sprites[SpriteType::PipeDecoration1];
	go[PipeDecorationIndex].z = 0.5f;
	go[PipeDecorationIndex].transparency = 255;
	int offsetX = 16;
	go[PipeDecorationIndex].x = PipePositionX - offsetX;
	go[PipeDecorationIndex].y = PipePositionY;

	PipeDecorationIndex++;

	go[PipeDecorationIndex].sprite = sprites[SpriteType::PipeDecoration2];
	go[PipeDecorationIndex].z = 0.5f;
	go[PipeDecorationIndex].transparency = 255;
	
	go[PipeDecorationIndex].x = PipePositionX - offsetX;
	go[PipeDecorationIndex].y = PipePositionY;

	PipeDecorationIndex++;

	go[PipeDecorationIndex].sprite = sprites[SpriteType::PipeDecoration3];
	go[PipeDecorationIndex].z = 0.5f;
	go[PipeDecorationIndex].transparency = 255;

	go[PipeDecorationIndex].x = PipePositionX - offsetX;
	go[PipeDecorationIndex].y = PipePositionY;
	

	PipeDecorationIndex++;
	go[PipeDecorationIndex].sprite = sprites[SpriteType::ChristmasTree1];
	go[PipeDecorationIndex].z = 0.5f;
	go[PipeDecorationIndex].transparency = 255;
	go[PipeDecorationIndex].type = 'T';
	go[PipeDecorationIndex].x = PipePositionX - 38;
	go[PipeDecorationIndex].y = 320;

}

void Game::WorldLogic()
{
	for (int i = GameObjectStartIndex::World; i < GameObjectLimitIndex::World; i++)
	{
		if (go[i].frame < go[i].sprite.cols)
		{
			go[i].frame += 0.003f;
		}

		if (go[i].frame > go[i].sprite.cols)
		{
			go[i].frame = 0;
		}
		
	}
}

double Game::getDistance(float dX0, float dY0, float dX1, float dY1)
{
	return sqrt((dX1 - dX0)*(dX1 - dX0) + (dY1 - dY0)*(dY1 - dY0));
}

float Game::getAngle(float dX0, float dY0, float dX1, float dY1)
{
	return atan2(dY0 - dY1, dX0 - dX1);
}

void Game::getStealPresentForGoomba()
{
	double minDistance=10000;
	double distance=0;
	int indexMinDistance;
	for (int goombaIndex = GameObjectStartIndex::Goomba; goombaIndex < GameObjectLimitIndex::Goomba; goombaIndex++)
	{
		if (go[goombaIndex].sprite.width == 0||go[goombaIndex].GoombaType==0||go[goombaIndex].StealPresentIndex>0)
			continue;

		for (int presentIndex = GameObjectStartIndex::Presents; presentIndex < GameObjectLimitIndex::Presents; presentIndex++)
		{
			if (go[presentIndex].moving&&go[presentIndex].stolen == false && go[presentIndex].y>200 && go[presentIndex].allowGoInPipe==false)
			{
				for (int goombaIndex = GameObjectStartIndex::Goomba; goombaIndex < GameObjectLimitIndex::Goomba; goombaIndex++)
				{
					if (go[goombaIndex].sprite.width == 0 || go[goombaIndex].GoombaType == 0 || go[goombaIndex].StealPresentIndex>0)
						continue;

					distance = getDistance(go[goombaIndex].x, go[goombaIndex].y, go[presentIndex].x, go[presentIndex].y);

					if (distance < minDistance)
					{
						minDistance = distance;
						indexMinDistance = goombaIndex;
					}
				}


				if (minDistance < 200)
				{

					go[indexMinDistance].StealPresentIndex = presentIndex;
					go[presentIndex].stolen = true;
				}
			
				return;
			}
		}

	}

}