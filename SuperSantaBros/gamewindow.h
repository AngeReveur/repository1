struct GAMEWINDOW
{
    HWND hWnd;
    int Width, Height;
    bool Windowed;

    GAMEWINDOW()
    {
        hWnd = NULL;
        Width = 1280;
        Height = 720;
        Windowed = true;
    }
};
