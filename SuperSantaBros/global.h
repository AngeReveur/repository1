// header files
#include <windows.h>
#include <windowsx.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <dinput.h>
#include "gamewindow.h"
#include "sprite.h"
#include "object.h"
#include "inputdata.h"
#include "game.h"
#include "gameenum.h"
#include<iostream>
#include<fstream>
#include <sstream>

// libraries
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
//#pragma comment (lib, "dinput.lib")
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")


// prototypes for...
// WinMain.cpp
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void DisplayWindow(Game::GAMEWINDOW* gw, HINSTANCE hInstance, int nCmdShow);
bool HandleMessages();
// Loop.cpp
void MainLoop();

//Util


